Source: aac-tactics
Section: math
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Stéphane Glondu <glondu@debian.org>,
           Julien Puydt <jpuydt@debian.org>
Build-Depends: coq (>= 8.17),
               debhelper-compat (= 13),
               dh-coq,
               dh-ocaml,
               libcoq-core-ocaml-dev,
               libcoq-stdlib,
               ocaml
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/coq-community/aac-tactics
Vcs-Browser: https://salsa.debian.org/ocaml-team/aac-tactics
Vcs-Git: https://salsa.debian.org/ocaml-team/aac-tactics.git

Package: libcoq-aac-tactics
Architecture: any
Breaks: libaac-tactics-coq
Replaces: libaac-tactics-coq, libaac-tactics-ocaml, libaac-tactics-ocaml-dev
Depends: ${coq:Depends}, ${ocaml:Depends}, ${misc:Depends}
Provides: aac-tactics, ${coq:Provides}
Description: Coq tactics for reasoning modulo AC (theories)
 This Coq plugin provides tactics for rewriting universally quantified
 equations, modulo associative (and possibly commutative) operators.
 .
 This package provides the Coq support library.
